const express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const bodyParser = require('body-parser');
const https = require('https');
const responseTime = require('response-time');
const fs = require('fs');
const usersRouter = require('./routes/user');
var db = require('./config/dbConfig');
const { MongoClient } = require('mongodb');
const novaNews = require('./model/novaNews');
const userModel = require('./model/userModel');
const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// Serve uploaded files
app.use('/uploads', express.static('uploads'));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");

    next();
});
app.use(express.urlencoded({ extended: true }))
/**
 * parse application/json
 */
app.use(express.json());
const expressValidator = require('express-validator');
app.use(expressValidator());  //this line to be addded
var cors = require('cors');
const { forgetPassword } = require('./services/userService');
app.use(responseTime());
app.use(cors())
app.use('/', usersRouter);

app.get('/', (req, res) => {
    res.json({ "message": "welcome to NOVA" });
});


const port = 3001;
app.listen(port, () => {
    console.log(`server running at http://localhost:${port}`);
});

const changeStream = novaNews.watch();
const request = require('request');

changeStream.on('change', async (change) => {
    console.log('Change detected:', change);
    // Handle the change here
    if (change.operationType == "insert") {
        console.log('Create');
        await userModel.find({ deviceId: { $exists: true, $ne: "" } })
            .then(async (users) => {
                console.log(users.length);
                users.forEach(user => {
                    const postData = {
                        mobileNo: user.mobileNo,
                        deviceId: user.deviceId,
                        type: 3,
                        title: change.fullDocument.title,
                        body: change.fullDocument.description,
                        _id: change.fullDocument._id,
                    };

                    const options = {
                        uri: `http://localhost:${port}/api/getNotification`,
                        method: 'POST',
                        json: postData,
                    };

                    request(options, (error, response, body) => {
                        if (!error && response.statusCode === 200) {
                            console.log('Response:', body);
                        } else {
                            console.error(error || body);
                        }
                    });
                });

            })
            .catch(err => {
                console.log(err);
            });
    }
});

module.exports = app;

