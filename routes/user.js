const express = require('express')
const router = express.Router();
const userController = require('../controllers/userController');
const patientController = require('../controllers/patientController');
const doctorController = require('../controllers/doctorController');
const novaNewsController = require('../controllers/novaNewsController');
const queryController = require('../controllers/queryController');
const { referPatient } = require('../services/userService');
const jwt = require('jsonwebtoken');
const middleware = require('../middleware/auth');
const novaNews = require('../model/novaNews');

const multer = require('multer');
const fs = require('fs');
const path = require('path');
const AWS = require('aws-sdk');
// Configure AWS SDK with your credentials
AWS.config.update({
    accessKeyId: 'AKIAWVIGVNMTTR3WKJVA',
    secretAccessKey: 'H7UO04O/+vbd7AmVJTO/lrsMIHCGUMbxwEcl0fBf',
    region: 'ap-south-1',
});

const s3 = new AWS.S3();

// Configure multer to handle file uploads
const s3storage = multer.memoryStorage();
const s3upload = multer({ storage: s3storage });

// Define a route to handle file uploads
router.post('/api/uploadNews', middleware.checkTokenAuth, s3upload.array('file', 2), (req, res) => {
    const files = req.files;

    if (!files || files.length < 2) {
        return res.status(400).json({ message: 'Two files are required' });
    }

    // Assuming the first file is for 'file1' and the second file is for 'file2'
    const [file1, file2] = files;

    const params1 = {
        Bucket: 'nova-ivf',
        Key: file1.originalname,
        Body: file1.buffer,
    };

    const params2 = {
        Bucket: 'nova-ivf',
        Key: file2.originalname,
        Body: file2.buffer,
    };

    // Upload the first file
    s3.upload(params1, (err1, data1) => {
        if (err1) {
            console.error('Error uploading first file to S3:', err1);
            return res.status(500).json({ message: 'Error uploading first file to S3' });
        }

        // Upload the second file
        s3.upload(params2, (err2, data2) => {
            if (err2) {
                console.error('Error uploading second file to S3:', err2);
                return res.status(500).json({ message: 'Error uploading second file to S3' });
            }

            console.log('Files uploaded successfully. S3 Object URLs:', data1.Location, data2.Location);
            const newsData = {
                title: req.body.title,
                description: req.body.description,
                url: data1.Location, // Update field names as needed
                image: data2.Location, // Update field names as needed
                publishedDate: new Date()
            };
            console.log(newsData);
            novaNews.create(newsData)
                .then(createdData => {
                    console.log("createdData");
                    console.log(createdData);
                    res.status(200).json({ message: 'Files uploaded successfully', data: createdData });
                })
                .catch(err => {
                    console.log("error");
                    console.log(err.message.response);
                    res.status(200).json({ message: err.message.response });
                });
        });
    });
});


// Create a function to generate user-specific upload folders
const getUserUploadFolder = (mobileNo) => {
    const uploadFolder = path.join('uploads', mobileNo);
    if (!fs.existsSync(uploadFolder)) {
        fs.mkdirSync(uploadFolder, { recursive: true });
    }
    return uploadFolder;
};

// Configure multer to save files to user-specific folders
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const mobileNo = req.params.mobileNo; // Extract user ID from route parameters
        const uploadFolder = getUserUploadFolder(mobileNo);
        cb(null, uploadFolder);
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname));
    },
});

const upload = multer({ storage: storage });

//user routes
router.post('/api/registration', userController.registration)
router.post('/api/myProfile', middleware.checkTokenAuth, userController.myProfile)
router.post('/api/login', userController.login)
router.post('/api/otpTrigger', userController.otpTrigger)
router.post('/api/referPatient', middleware.checkTokenAuth, patientController.referPatient)
router.post('/api/updateUser', middleware.checkTokenAuth, userController.updateUser)
router.post('/api/deleteUser', middleware.checkTokenAuth, userController.deleteUser)
router.post('/api/trackPatient', middleware.checkTokenAuth, patientController.trackPatient)
router.post('/api/trackPatientDetails', middleware.checkTokenAuth, patientController.trackPatientDetails)
router.post('/api/allQuery', middleware.checkTokenAuth, queryController.allQuery)
router.post('/api/forgetPassword', userController.forgetPassword)
router.post('/api/createQuery', userController.createQuery)
router.get('/api/getDoctorDetailsBymobileNo', middleware.checkTokenAuth, doctorController.getDoctorDetailsBymobileNo)
router.post('/api/notification', userController.notification)
router.post('/api/pushNotification', userController.pushNotification)
router.post('/api/getNotification', userController.getNotification)
router.post('/api/getNovaNewsNotification', userController.getNovaNewsNotification)

router.get('/api/getAllNews', middleware.checkTokenAuth, novaNewsController.getAllNews)
// router.post('/api/uploadProfilePicToAzure', middleware.checkTokenAuth,userController.uploadProfilePicToAzure)
router.post('/api/getProfilePic/:mobileNo', upload.single('image'), middleware.checkTokenAuth, userController.getProfilePic)

router.post('/api/loginByDeviceId', userController.loginByDeviceId)
// Get Query Response from leadsquare API
router.post('/api/getQueryResponse', queryController.getQueryResponse)
module.exports = router;
