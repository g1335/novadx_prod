const jwt = require('jsonwebtoken');
const logger = require('../util/logger');

exports.checkTokenAuth = (req, res, next) => {
    try {
        var token1 = req.headers['authorization'];
        if (token1) {
            //bypass the token with a pwd
            if (token1 == 'nova#123') {
                next();
            } else {
                //verifies secret and checks expression
                jwt.verify(token1, 'secretkey-auth', (err, decoded) => {
                    if (err) {
                        return res.status(401).send({
                            status: false,
                            message: 'Unauthorised access, please provide valid token!'
                        });
                    } else {
                        req.decoded = decoded;
                        next();
                    }
                });
            }
        } else {
            //if there is no token return an error
            return res.send({
                status: false,
                message: 'No token provided!!'
            });
        }
    } catch (err) {
        logger.log({
            level: 'error',
            message: err.message,
            method: 'checkTokenAuth'
        });
    }
}