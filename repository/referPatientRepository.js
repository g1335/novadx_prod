var mongodb = require('mongodb');
var logger = require('morgan');
const referPatient = require('../model/referPatient');
const axios = require('axios');

exports.referPatient = async (req, callBack) => {
  var docNumber = req.body.docNumber;
  var docName = req.body.docName;
  var name = req.body.name;
  var age = req.body.age;
  var gender = req.body.gender;
  var mobileNo = req.body.mobileNo;
  var email = req.body.email;
  var city = req.body.city;
  var preferreddoctorremarks = req.body.preferreddoctorremarks;


  let data = JSON.stringify({
    "LeadDetails": [
      {
        "Attribute": "Phone",
        "Value": docNumber
      },
      {
        "Attribute": "FirstName",
        "Value": docName
      },
      {
        "Attribute": "SearchBy",
        "Value": "phone"
      }
    ],
    "Activity": {
      "ActivityEvent": "213",
      "ActivityNote": preferreddoctorremarks,
      "ActivityDateTime": "2023-03-29 14:50:50",
      "Fields": [
        {
          "SchemaName": "mx_Custom_1",
          "Value": name
        },
        {
          "SchemaName": "mx_Custom_2",
          "Value": age
        },
        {
          "SchemaName": "mx_Custom_3",
          "Value": gender
        },
        {
          "SchemaName": "mx_Custom_4",
          "Value": mobileNo
        },
        {
          "SchemaName": "mx_Custom_5",
          "Value": email
        },
        {
          "SchemaName": "mx_Custom_6",
          "Value": city
        }
      ]
    }
  });

  let config = {
    method: 'post',
    maxBodyLength: Infinity,
    url: 'https://api-in21.leadsquared.com/v2/ProspectActivity.svc/CreateCustom?accessKey=u$r00eb794494d307e0f7dc5e19ada78add&secretKey=26d75aeae9e4df9ffe785b89aa366b626759974f',
    headers: {
      'Content-Type': 'application/json'
    },
    data: data
  };

  axios.request(config)
    .then((response) => {
      console.log(JSON.stringify(response.data));
      // return callBack(null, response.data);
    })
    .catch((error) => {
      console.log(error);
      return callBack(error.message);
    });
      // check if user already exist
  await referPatient.find({ mobileNo: mobileNo})
  .then(count => {
    if (count.length > 0) {
      callBack('Patient already exist')
    }
    else {
      const referPatientData = {
        docNumber: docNumber,
        name: name,
        age: age,
        gender: gender,
        mobileNo: mobileNo,
        email: email,
        city: city,
        preferreddoctorremarks: preferreddoctorremarks      
      };
      referPatient.create(referPatientData)
        .then(data => {
          console.log("Referred Patient successful!");
          console.log("Mobile number: " + mobileNo);
          var result = {
            createdUserId: data.id,
          }
          createdUserId = data.id;
          console.log(result);
          return callBack(null, result);
        })
        .catch(err => {
        //   logger.log({
        //     level: 'error',
        //     message: err.message,
        //     method: 'registration'
        //   });
          // console.log(err.message, "error while referring patient");
          return callBack(err.message);
        });
    }
  })
  .catch(err => {
    // logger.log({
    //   level: 'error',
    //   message: err.message,
    //   method: 'registration'
    // });
    callBack(err.message)
  })

};