const novaNews=require('../model/novaNews');
var mongodb=require('mongoose');
var logger = require('morgan');

exports.getAllNews = async (req, callBack) => {

   await novaNews.find().sort({ _id: 1 })
  .then(result => {
   
    if (result.length > 0) {
      return callBack(null,result);
    }else{
      callBack('News not exist');
    }
  })
  .catch(err => {
    // logger.log({
    //   level: 'error',
    //   message: err.message,
    //   method: 'registration'
    // });
    callBack(err.message)
  })
  };
        