// const login = require("../model/login");
var mongodb = require('mongodb');
var logger = require('morgan');
const User = require('../model/userModel');
const query = require('../model/query');
const allQuery = require('../model/allQuery');
var moment = require('moment');
const { error } = require('winston');
const Notification = require("../model/notification")
const pushNotification = require("../model/pushNotification")
// var password=require
const { admin } = require('../firebase-configurations/firebase-config');
const firebaseDb = admin.firestore();
const notification_options = {
  priority: "high",
  timeToLive: 60 * 60 * 24
};
const axios = require('axios');
const fs = require('fs');
const https = require('https');
// Example OTP data store (replace with your actual data store)
const otpStore = {};

exports.otpTrigger = async (req, callBack) => {
  try {
    const mobileNo = req.body.mobileNo;
    // Check if an OTP already exists for the mobile number
    if (otpStore[mobileNo]) {
      return callBack('An OTP has already been sent. Please wait for it to expire');
    }
  
    // Generate a dynamic OTP
    // const otp = Math.floor(1000 + Math.random() * 9000);
    const otp = 1234;
    console.log(otp);

    // Send OTP via SMS
    const url = `http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=${mobileNo}&msg=Use OTP ${otp} to login to Nova app. This is valid only for 2 minutes and can be used only once. - Nova IVF Fertility&msg_type=TEXT&userid=2000231656&auth_scheme=plain&password=Y5d6g55Cg&v=1.1&format=text`;

    // ... your Axios request code here to send the OTP
    let config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: url, // Use the constructed URL here
      headers: {}
    };

    axios
      .request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
        return callBack(error.message);
      });

    // Store the OTP and set a timer for 2 minutes (in milliseconds)
    otpStore[mobileNo] = otp;
    const expirationTime = 2 * 60 * 1000; // 2 minutes
    setTimeout(() => {
      // Remove the OTP from the store after expiration
      delete otpStore[mobileNo];
    }, expirationTime);

    // Send the OTP (in a real application, you would send it via SMS or another channel)
    console.log(`OTP sent to ${mobileNo}: ${otp}`);
    return callBack(null, 'OTP Sent Successful!');
  } catch (error) {
    console.error(error);
    return callBack(error.message);
  }
};

exports.login = async (req, callBack) => {
  var result = {};
  const mobileNo = req.body.mobileNo;
  const otp = req.body.otp;

  // Check if an OTP exists for the mobile number
  if (!otpStore[mobileNo]) {
    return callBack('No OTP found for this mobile number.');
  }

  // Verify if the provided OTP matches the stored OTP
  console.log(otpStore[mobileNo] , parseInt(otp))
  if (otpStore[mobileNo] === parseInt(otp)) {
    // OTP is valid; you can perform further actions here
    delete otpStore[mobileNo]; // Remove the OTP from the store
    // return res.status(200).json({ message: 'OTP verified successfully.' });
    var newvalues = { $set: { deviceId: req.body.deviceId, otp: "" } };

    postdata = {
      mobileNo: mobileNo,
      // otp: otp,


    }
    console.log(postdata)
    User.find({deviceId : req.body.deviceId})
      .then((users) => {
        console.log(users)
       
        users.forEach(element => {
          if(req.body.mobileNo != element.mobileNo){
            User.findOne({mobileNo: element.mobileNo,deviceId : element.deviceId})
          .then((childuser) => {
            console.log(childuser)
           
    
              // User exists; update the desired column value (e.g., update 'isActive' to true)
              childuser.deviceId = ""; // Replace with the column you want to update
              // user.otp = ""; // Replace with the column you want to update
    
              // Save the updated user document
              childuser.save();
    
          })
          .catch(err => {
            console.log(err.message);
            // return callBack(err.message);
          });
          }
        });

      })
      .catch(err => {
        console.log(err.message);
        // return callBack(err.message);
      });


    User.findOne(postdata)
      .then((user) => {
        console.log(user)
        if (!user) {
          return callBack("Mobile No is wrong!");
        }
        else {
          // Assuming req.body.deviceId contains the new deviceId value
          const newDeviceId = req.body.deviceId;

          // Construct the update query
          const filter = { mobileNo: req.body.mobileNo }; // Replace with your filter criteria
          const update = { deviceId: newDeviceId };

          // User exists; update the desired column value (e.g., update 'isActive' to true)
          user.deviceId = newDeviceId; // Replace with the column you want to update
          // user.otp = ""; // Replace with the column you want to update

          // Save the updated user document
          user.save();

          console.log("Login Successful!");
          console.log("mobile number: " + user);
          result.mobileNo = user.mobileNo;

          return callBack(null, result);

        }

      })
      .catch(err => {
        console.log(err.message);
        return callBack(err.message);
      });

  } else {
    return callBack('Invalid OTP.');
  }

};
exports.registration = async (req, callBack) => {
  var firstName = req.body.firstName;
  var dateofBirth = req.body.dateofBirth;
  var gender = req.body.gender;
  var mobileNo = req.body.mobileNo;
  var email = req.body.email;
  var password = req.body.password;
  var reenterPassword = req.body.reenterPassword;
  var hashPass = '';


  //calculate age
  const today = new Date();
  var formattedDOB = moment(moment(dateofBirth, 'DD-MM-YYYY')).format('YYYY-MM-DD');
  const birthDate = new Date(formattedDOB);

  var yearsDifference = today.getFullYear() - birthDate.getFullYear();

  if (
    today.getMonth() < birthDate.getMonth() ||
    (today.getMonth() === birthDate.getMonth() && today.getDate() < birthDate.getDate())
  ) {
    yearsDifference = yearsDifference - 1;
  }
  else {
    yearsDifference = yearsDifference;
  }

  if (password !== reenterPassword) {
    return callBack('Passwords must be same')
  }
  //check if user already exist
  await User.find({ mobileNo: mobileNo })
    .then(count => {
      console.log(count);
      if (count.length > 0) {
        callBack('User already exist')
      }
      else {
        const userData = {
          firstName: firstName,
          dateofBirth: dateofBirth,
          gender: gender,
          mobileNo: mobileNo,
          email: email,
          password: password,
        };
        console.log(userData);
        User.create(userData)
          .then(data => {
            console.log("Registration successful!");
            console.log("Mobile number: " + mobileNo);
            var result = {
              createdUserId: data.id,
            }
            createdUserId = data.id;
            return callBack(null, result);
          })
          .catch(err => {
            //   logger.log({
            //     level: 'error',
            //     message: err.message,
            //     method: 'registration'
            //   });
            // console.log(err.message, "error while registration");
            return callBack(err.message);
          });
      }
    })
    .catch(err => {
      // logger.log({
      //   level: 'error',
      //   message: err.message,
      //   method: 'registration'
      // });
      callBack(err.message)
    })
};

exports.myProfile = async (req, callBack) => {

  var mobileNo = req.body.mobileNo;


  await User.find({ mobileNo: mobileNo })
    .then(result => {
      if (result.length > 0) {
        return callBack(null, result);
      } else {
        callBack('User not exist');
      }


    })
    .catch(err => {
      // logger.log({
      //   level: 'error',
      //   message: err.message,
      //   method: 'registration'
      // });
      callBack(err.message)
    })
};

exports.forgetPassword = async (req, callBack) => {

  var mobileNo = req.body.mobileNo;
  var otp = req.body.otp;
  var newPassword = req.body.newPassword;
  var confirmPassword = req.body.confirmPassword

  if (newPassword !== confirmPassword) {
    return callBack('Passwords must be same')
  }
  // check if user already exist
  await User.find({ mobileNo: mobileNo }, { otp: otp })
    .then(count => {
      if (!count.length > 0) {
        callBack('user mobile number and otp is not match or mobile number not is exist.!')
      }
      else {
        const userData = {
          // mobileNo: mobileNo,
          password: newPassword,
          // confirmPassword: confirmPassword,    
        };
        User.findOneAndUpdate(
          { mobileNo: mobileNo },
          userData,
        )
          .then(userData => {
            // console.log("password updated successfuly!");
            return callBack(null, "password updated successfuly!");
          })
          .catch(err => {
            return callBack(err.message);
          });

      }
    })
    .catch(err => {
      // logger.log({
      //   level: 'error',
      //   message: err.message,
      //   method: 'registration'
      // });
      callBack(err.message)
    })
};

exports.updateUser = async (req, callBack) => {

  var firstName = req.body.firstName;
  var dateofBirth = req.body.dateofBirth;
  var email = req.body.email;
  var gender = req.body.gender;
  var mobileNo = req.body.mobileNo;
  var temp = req.body;
  var body = { $set: temp };


  await User.find({ mobileNo: mobileNo })
    .then(result => {
      if (!result.length > 0) {
        return callBack('User Not exist!.');
      } else {
        // return callBack(null,'Updated Successfully!.');
        User.updateOne({ mobileNo: mobileNo }, body).then(data => {
          // console.log(data);
          return callBack(null, 'Updated Successfully!.');
        })
          .catch(err => {
            //   logger.log({
            //     level: 'error',
            //     message: err.message,
            //     method: 'registration'
            //   });
            return callBack(err.message);
          });

      }

    })
    .catch(err => {
      // logger.log({
      //   level: 'error',
      //   message: err.message,
      //   method: 'registration'
      // });
      callBack(err.message)
    })
};
exports.deleteUser = async (req, callBack) => {

  var mobileNo = req.body.mobileNo;

  await User.find({ mobileNo: mobileNo })
    .then(result => {
      if (!result.length > 0) {
        return callBack('User Not exist!.');
      } else {
        User.deleteOne({ mobileNo: mobileNo }).then(data => {
          return callBack(null, 'User Deleted Successfully!.');
        })
          .catch(err => {
            //   logger.log({
            //     level: 'error',
            //     message: err.message,
            //     method: 'registration'
            //   });
            return callBack(err.message);
          });

      }

    })
    .catch(err => {
      // logger.log({
      //   level: 'error',
      //   message: err.message,
      //   method: 'registration'
      // });
      callBack(err.message)
    })
};

exports.createQuery = async (req, callBack) => {
  var docNumber = req.body.docNumber;
  var docName = req.body.docName;
  var queryType = req.body.queryType;
  var queryContent = req.body.queryContent;


  let data = JSON.stringify({
    "LeadDetails": [
      {
        "Attribute": "Phone",
        "Value": docNumber
      },
      {
        "Attribute": "FirstName",
        "Value": docName
      },
      {
        "Attribute": "SearchBy",
        "Value": "phone"
      }
    ],
    "Activity": {
      "ActivityEvent": "213",
      "ActivityNote": "Note for the activity",
      "ActivityDateTime": "2023-03-29 14:50:50",
      "Fields": [
        {
          "SchemaName": "mx_Custom_1",
          "Value": queryType
        },
        {
          "SchemaName": "mx_CustomObject_1",
          "Value": queryContent,
        }
      ]
    }
  });

  let config = {
    method: 'post',
    maxBodyLength: Infinity,
    url: 'https://api-in21.leadsquared.com/v2/ProspectActivity.svc/CreateCustom?accessKey=u$r00eb794494d307e0f7dc5e19ada78add&secretKey=26d75aeae9e4df9ffe785b89aa366b626759974f',
    headers: {
      'Content-Type': 'application/json'
    },
    data: data
  };

  axios.request(config)
    .then((response) => {
      console.log(JSON.stringify(response.data));
      const queryData = {
        docNumber: docNumber,
        docName: docName,
        queryType: queryType,
        queryContent: queryContent,
        leadSqrCreateId: "00",
      };
      console.log(queryData);
      allQuery.create(queryData)
        .then(data => {
          console.log("data");
          console.log(data);
          return callBack(null, response.data);
        })
        .catch(err => {
          console.log("error");
          console.log(err.message.response);
          return callBack(err.message);
        });

    })
    .catch((error) => {
      console.log(error);
      return callBack(error.message);
    });


  // await query.find({ mobileNo: mobileNo })
  // .then(count => {
  //   console.log(count);


  //     const queryData = {

  //       mobileNo: mobileNo,
  //       queryType: queryType,
  //       queryContent: queryContent     
  //     };


  //     query.create(queryData)
  //       .then(data => {
  //         // console.log("query successful!");
  //         // console.log("Mobile number: " + mobileNo);
  //         var result = {
  //           createdUserId: data.id,
  //         }
  //         createdUserId = data.id;
  //         return callBack(null, 'created successfully');
  //       })
  //       .catch(err => {
  //       //   logger.log({
  //       //     level: 'error',
  //       //     message: err.message,
  //       //     method: 'registration'
  //       //   });
  //         // console.log(err.message, "error while query");
  //         return callBack(err.message);
  //       });

  // })
  // .catch(err => {
  //   // logger.log({
  //   //   level: 'error',
  //   //   message: err.message,
  //   //   method: 'registration'
  //   // });
  //   callBack(err.message)
  // })
};

// exports.uploadProfilePicToAzure = async (body, callBack) => {
//   try {
//     var result = {};
//     var mobileNo = body.body.mobileNo;
//     await User.find({ mobileNo: mongodb.ObjectId(mobileNo) })
//       .then(async (user) => {
//         if (user.length == 0) {
//           return callBack("User Not found!");
//         }
//         else {
//           const blobServiceClient = await reusable.azureBlobServiceClientBuilder();
//           const containerName = process.env.AZURE_STORAGE_PROFILE_PIC_CONTAINER_NAME;
//           const containerClient = blobServiceClient.getContainerClient(containerName);
//           var mimtype = body.file.mimetype.split("/");
//           const fileType = mimtype[mimtype.length - 1];
//           var fileExtension = "." + fileType;
//           const content = body.file.buffer;
//           const blobName = userId + fileExtension;
//           const blockBlobClient = containerClient.getBlockBlobClient(blobName);
//           const uploadBlobResponse = await blockBlobClient.upload(content, content.length);
//           console.log(`Upload block blob ${blobName} successfully`, uploadBlobResponse.requestId);
//           const profileblobClient = containerClient.getBlobClient(blobName);
//           var data = {
//             profilePic: profileblobClient.url
//           };
//           User.findOneAndUpdate(
//             { _id: mongodb.ObjectId(mobileNo) },
//             data,
//           )
//             .then(data => {
//               console.log("profilePic updated!");
//             })
//             .catch(err => {
//               logger.log({
//                 level: 'error',
//                 message: err.message,
//                 method: 'uploadProfilePicToAzure'
//               });
//               console.log(err.message, "catch error");
//               return callBack(err.message);
//             });
//           return callBack(null, profileblobClient.url);
//         }
//       })
//       .catch(err => {
//         logger.log({
//           level: 'error',
//           message: err.message,
//           method: 'uploadProfilePicToAzure'
//         });
//         console.log(err.message, "catch error");
//         return callBack(err.message);
//       });
//   } catch (err) {
//     console.log(err.message);
//     logger.log({
//       level: 'error',
//       message: err.message,
//       method: 'uploadProfilePicToAzure'
//     });
//     return callBack(err.message)
//   }
// }

exports.getProfilePic = async (req, callBack) => {
  try {
    if (!req.file) {
      return callBack('No file uploaded.')
    }

    // Process the uploaded file as needed (e.g., save it to a database)

    // Convert backslashes to forward slashes and concatenate with the base URL
    const imageUrl = `${req.file.path.replace(/\\/g, '/')}`;
    const destinationUrl = `${req.file.destination.replace(/\\/g, '/')}`;

    const mobileNo = req.body.mobileNo;
    postdata = {
      mobileNo: mobileNo
    }
    var newvalues = { $set: { image: imageUrl } };
    console.log(postdata)
    await User.find(postdata)
      .then(async (user) => {
        console.log(user)
        if (user.length == 0) {
          fs.rmdir(destinationUrl, { recursive: true }, (err) => {
            if (err) {
              console.error('Error deleting folder:', err);
            } else {
              console.log('Folder deleted successfully.');
            }
          });
          return callBack("User not verified!");
        }
        else {
          User.findOneAndUpdate({ mobileNo: mobileNo }, newvalues).then(data => {
            console.log(data);
            // return callBack(null,'Updated Successfully!.');
            console.log(imageUrl);
            return callBack(null, data);
          })

        }

      })
      .catch(err => {
        // Use the fs.rmdir() function to remove the folder and its contents
        fs.rmdir(destinationUrl, { recursive: true }, (err) => {
          if (err) {
            console.error('Error deleting folder:', err);
          } else {
            console.log('Folder deleted successfully.');
          }
        });
        return callBack(err.message);
      });

  } catch (err) {
    console.log(err.message);
    // logger.log({
    //   level: 'error',
    //   message: err.message,
    //   method: 'getProfilePic'
    // });
    return callBack(err.message)
  }
}

exports.notification = async (req, callBack) => {
  var registrationToken = req.body.registrationToken;
  // var userName = req.body.userName;
  var mobileNo = parseInt(req.body.mobileNo);
  var number = req.body.mobileNo;
  var deviceId = req.body.deviceId;
  var appName = req.body.appName;
  var model = req.body.model;
  var appId = req.body.model;
  var data = {
    //  userName:userName,
    mobileNo: mobileNo,
    deviceId: deviceId,
    appName: appName,
    model: model,
    appId: appId,
    registrationToken: registrationToken
  }
  console.log(data);
  console.log(req.body)
  const empRef = firebaseDb.collection('notificationmasters').doc(number);
  // Mongo DB
  async function updateMongoDb(data, cb) {
    let result = {}
    await Notification.findOneAndUpdate(
      { mobileNo: data.mobileNo },
      data,).then(res => {
        result.statusCode = 100
        result.status = true;
        result.message = "Data updated successfully in Mongo..";
        result.document = res;
        return cb(result);
      }).catch(e => {
        result.statusCode = 500;
        result.status = false;
        result.message = "Error While Updating the Mongo Collection";
        result.document = JSON.stringify(e);
        return cb(result);
      })
  }
  async function createMongoDb(data, cb) {
    let result = {}
    await Notification.create(data).then(async (r) => {
      result.statusCode = 100
      result.status = true;
      result.message = "Data Created successfully in Mongo..";
      result.document = JSON.stringify(r);
      return cb(result);
    }).catch((e) => {
      result.statusCode = 500;
      result.status = false;
      result.message = "Error While Creating the Mongo Collection";
      result.document = JSON.stringify(e);
      return cb(result);
    })
  }
  async function checkMongoDb(data, cb) {
    let result = {}
    await Notification.find({ mobileNo: data.mobileNo })
      .then(async (notification) => {
        if (notification.length > 0) {
          result.statusCode = 100
          result.status = true;
          result.message = "User exists"
          result.document = notification[0];
        } else {
          result.statusCode = 100
          result.status = false
          result.message = "No data found";
          result.document = {};
        }
        return cb(result);
      }).catch(e => {
        result.statusCode = 500;
        result.status = false;
        result.message = "Error While Checking the Data";
        result.document = JSON.stringify(e);
        return cb(result);
      })
  }
  // Cloud Firestore
  async function updateFirebase(data, cb) {
    let result = {}
    await empRef.update({ data }).then((r) => {
      result.statusCode = 100
      result.status = true;
      result.message = "Data updated successfully in Firebase..";
      result.document = JSON.stringify(r);
      return cb(result);
    }).catch(e => {
      result.statusCode = 500;
      result.status = false;
      result.message = "Error While Updating the Firebase Collection";
      result.document = JSON.stringify(e);
      return cb(result);
    })

  }
  async function saveUserToken(data, cb) {
    try {
      const res = await reusable.sendFbId(data);
    } catch (err) {
      console.log(err)
    }
  }
  async function createFirebase(data, cb) {
    let result = {}
    await empRef.set({ data }).then(async (r) => {
      result.statusCode = 100
      result.status = true;
      result.message = "Data inserted successfully in Firebase..";
      result.document = JSON.stringify(r);
      return cb(result);
    }).catch((e) => {
      result.statusCode = 500;
      result.status = false;
      result.message = "Error While Creating the Firebase Collection";
      result.document = JSON.stringify(e);
      return cb(result);
    })
  }
  try {
    await checkMongoDb(data, async function (notification) {
      if (notification.statusCode === 500) return callBack(notification);
      if (notification.status) {
        console.log(notification.document.registrationToken, "===", registrationToken);
        if (notification.document.registrationToken === registrationToken) {
          return callBack(null, notification);
        } else {
          await updateMongoDb(data, async function (updateMongo) {
            if (updateMongo.statusCode === 500) return callBack(updateMongo);
            await updateFirebase(data, async function (updateFirebaseDb) {
              await saveUserToken(data, async function (saveUserToken) {
                if (saveUserToken.statusCode === 500) return callBack(saveUserToken);
              })
              if (updateFirebaseDb.statusCode === 500) return callBack(updateFirebaseDb);
              response = {}
              response.mongo = updateMongo
              response.firebase = updateFirebaseDb
              return callBack(null, response)
            });
          });
        }
      } else {
        await createMongoDb(data, async function (createMongo) {
          if (createMongo.statusCode === 500) return callBack(createMongo);
          await createFirebase(data, async function (createFirebaseDb) {
            await saveUserToken(data, async function (saveUserToken) {
              if (saveUserToken.statusCode === 500) return callBack(saveUserToken);
            })
            if (createFirebaseDb.statusCode === 500) return callBack(createFirebaseDb);
            res = {}
            res.mongo = createMongo
            res.firebase = createFirebaseDb
            return callBack(null, res)
          });
        });
      }
    })
  } catch (err) {
    console.log("error", err);
    return callBack(err.message);
  }
}

exports.pushNotification = async (req, callBack) => {
  try {
    // var OTP = Math.floor(100000 + Math.random() * 900000);
    var mobileNo = req.body.mobileNo;
    var deviceId = req.body.deviceId;
    var type = req.body.type;
    var title = '';
    var body = '';
    var registrationToken = '';
    if (type == 1) {
      title = "Refer Patient";
      body = "Your patient has been referred successfully.";
    } else if (type == 2) {
      title = "Query Received";
      body = "test query";
    }else if (type == 3) {
      title = req.body.title;
      body = req.body.body;
    }
    // Notification.findOne({ mobileNo: mobileNo },{ sort: { createdAt: -1 } },(err, nData) => {
    //   console.log(nData);
    //   registrationToken = nData.registrationToken;
    //   },
    // );    

    await Notification.find({ mobileNo: mobileNo }).sort({ "createdAt": -1 }).limit(1)
      .then(async (user) => {
        console.log(user);
        console.log(user[0]);
        console.log(user[0].registrationToken);
        console.log(title);
        console.log(body);
        registrationToken = user[0].registrationToken;
      })
      .catch(err => {
        // logger.log({
        //   level: 'error',
        //   message: err.message,
        //   method: 'getProfilePic'
        // });
        return callBack(err.message)
      });
    var message;
    message = {
      notification: {
        title: title,
        body: body,
        // image:image
      },
      // data:{
      //     page:data
      // }
    };
    console.log(registrationToken);
    const options = notification_options;
    admin.messaging().sendToDevice(registrationToken, message, options)
      .then(response => {
        const nData = {
          mobileNo: mobileNo,
          deviceId: deviceId,
          type: type,
          status: 1,
          typeNotificationID: req.body._id
        };
        console.log(nData);
        pushNotification.create(nData)
          .then(data => {
            console.log(data);
            console.log("push notification record created successful!");
          })
          .catch(err => {
            return callBack(err.message);
          });
        console.log("Notification sent successfully...", response.results);
        console.log("results", response.results[0].results);
        return callBack(null, response.results[0].results)

      })
      .catch(error => {
        console.log("error", error);
        return callBack(error.message);
      });
  }
  catch (err) {
    console.log("error", err);
    return callBack(err.message);
  }

}

exports.loginByDeviceId = async (req, callBack) => {
  // var result = {};

  try {
    await User.find({ deviceId: req.body.deviceId })
      .then(result => {
        console.log(result);
        if (!result.length > 0) {
          return callBack("deviceId doesn't exist!");
        }
        else {

          console.log("Login Successful!");
          console.log(result);
          result.mobileNo = result.mobileNo;
          return callBack(null, result);
        }
      })
      .catch(err => {
        // logger.log({
        //   level: 'error',
        //    message: err.message,
        //    method: 'loginNew'
        //  });
        return callBack(err.message);
      });

  } catch (err) {
    console.log(err.message);
    // logger.log({
    //   level: 'error',
    //   message: err.message,
    //   method: 'getProfilePic'
    // });
    return callBack(err.message)
  }
};

