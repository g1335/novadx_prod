const allQuery = require('../model/allQuery');
var mongodb = require('mongoose');
var logger = require('morgan');

exports.getQueryResponse = async (req, callBack) => {
  try {
    var temp = req.body;
    var body = { $set: {queryResponse:req.body.response} };
    await allQuery.find({ leadSqrCreateId: req.body.id })
    .then(result => {
      if (!result.length > 0) {
        return callBack('Query Not exist!.');
      } else {
        // return callBack(null,'Updated Successfully!.');
        allQuery.updateOne({ leadSqrCreateId: req.body.id }, body).then(data => {
          // console.log(data);
          return callBack(null, 'Response sent Successfully!.');
        })
          .catch(err => {
            //   logger.log({
            //     level: 'error',
            //     message: err.message,
            //     method: 'registration'
            //   });
            return callBack(err.message);
          });

      }

    })
    .catch(err => {
      // logger.log({
      //   level: 'error',
      //   message: err.message,
      //   method: 'registration'
      // });
      callBack(err.message)
    })
  } catch (err) {
    console.log("error", err.message);
    // logger.log({
    //     level: 'error',
    //     message: err.message,
    //     method: 'getDoctorDetailsBymobileNo'
    // });
    callBack(err.message);
  }
}

exports.allQuery = async (req, callBack) => {
  var mobileNo = req.body.mobileNo;

  try {
    await allQuery.find({ docNumber: mobileNo })
      .then(result => {
        console.log("result");
        console.log(result);
        if (result.length > 0) {
          return callBack(null, result);
        } else {
          callBack('Query not exist');
        }


      })
      .catch(err => {
        // logger.log({
        //   level: 'error',
        //   message: err.message,
        //   method: 'registration'
        // });
        callBack(err.message)
      })
  } catch (err) {
    console.log("error", err.message);
    // logger.log({
    //     level: 'error',
    //     message: err.message,
    //     method: 'getDoctorDetailsBymobileNo'
    // });
    callBack(err.message);
  }
};
