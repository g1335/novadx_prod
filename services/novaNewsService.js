const repo=require('../repository/novaNewsRepository.js');

exports.getAllNews = (req, callback) => {
    repo.getAllNews(req, (err, result) => {
        if (err) {
            return callback(err)
        } else {
            return callback(null, result);
        }
    })
}