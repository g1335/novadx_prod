const repo=require('../repository/allQuery.js');

// getQueryResponse
exports.getQueryResponse=(req,callBack)=>{
    repo.getQueryResponse(req,(err,result)=>{
        if (err){
       return callBack(err);
     }else{
         return callBack(null,result)
   }
 })  
}
exports.allQuery = (req, callback) => {
    repo.allQuery(req, (err, result) => {
        if (err) {
            return callback(err)
        } else {
            return callback(null, result);
        }
    })
}