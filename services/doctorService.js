const repo=require('../repository/doctorRepository.js');

exports.getDoctorDetailsBymobileNo = (req, callback) => {
    repo.getDoctorDetailsBymobileNo(req, (err, result) => {
        if (err) {
            return callback(err)
        } else {
            return callback(null, result);
        }
    })
}