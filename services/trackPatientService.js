const repo = require('../repository/trackPatientRepository')

exports.trackPatient = (req ,callBack) => {
    repo.trackPatient(req, (err, result) => {
        if (err) {
            return callBack(err);
        } else {
            return callBack(null, result)
        }
    })

}