const repo = require('../repository/userRepository')

exports.otpTrigger = (req ,callBack) => {
    repo.otpTrigger(req, (err, result) => {
        if (err) {
            return callBack(err);
        } else {
            return callBack(null, result)
        }
    })
}

exports.login = (req ,callBack) => {
    repo.login(req, (err, result) => {
        if (err) {
            return callBack(err);
        } else {
            return callBack(null, result)
        }
    })
}
exports.registration = (req, callBack) => {
    repo.registration(req, (err, result) => {
        if (err) {
            callBack(err);
        } else {
            return callBack(null, result)
        }
    })
}


exports.forgetPassword=(req,callBack)=>{
     repo.forgetPassword(req,(err,result)=>{
         if (err){
        callBack(err);
      }else{
          return callBack(null,result)
    }
  })  
}

exports.myProfile=(req,callBack)=>{
    repo.myProfile(req,(err,result)=>{
        if (err){
       callBack(err);
     }else{
         return callBack(null,result)
   }
 })  
}

exports.updateUser=(req,callBack)=>{
    repo.updateUser(req,(err,result)=>{
        if (err){
       callBack(err);
     }else{
         return callBack(null,result)
   }
 })  
}

exports.deleteUser=(req,callBack)=>{
    repo.deleteUser(req,(err,result)=>{
        if (err){
       callBack(err);
     }else{
         return callBack(null,result)
   }
 })  
}

exports.createQuery=(req,callBack)=>{
    repo.createQuery(req,(err,result)=>{
        if (err){
       return callBack(err);
     }else{
         return callBack(null,result)
   }
 })  
}


// exports.uploadProfilePicToAzure = (req, callback) => {
//     repo.uploadProfilePicToAzure(req, (err, result) => {
//         if (err) {
//             return callback(err)
//         } else {
//             return callback(null, result);
//         }
//     })
// }
exports.getProfilePic = (req, callback) => {
    repo.getProfilePic(req, (err, result) => {
        if (err) {
            return callback(err)
        } else {
            return callback(null, result);
        }
    })
}

exports.notification = (req, callback) => {
    repo.notification(req, (err, result) => {
        if (err) {
            return callback(err)
        } else {
            return callback(null, result);
        }
    })
}
exports.pushNotification = (req, callback) => {
    repo.pushNotification(req, (err, result) => {
        if (err) {
            return callback(err)
        } else {
            return callback(null, result);
        }
    })
}

exports.getNotification = (req, callback) => {
    repo.pushNotification(req, (err, result) => {
        if (err) {
            return callback(err)
        } else {
            return callback(null, result);
        }
    })
}

exports.loginByDeviceId = (req ,callBack) => {
        repo.loginByDeviceId(req, (err, result) => {
            if (err) {
                return callBack(err);
            } else {
                return callBack(null, result)
            }
        })
    }

