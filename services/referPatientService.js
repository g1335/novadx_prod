const repo = require('../repository/referPatientRepository')

exports.referPatient = (req, callBack) => {
    repo.referPatient(req, (err, result) => {
        if (err) {
            callBack(err);
        } else {
            return callBack(null, result)
        }
    })
}