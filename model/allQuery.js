var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var allQuerySchema = new Schema({
              docNumber:{type:String},
              docName: {type:String},
              queryType: {type:String},
              queryContent:{type:String},
              queryResponse:{type:String},
              queryResponse:{type:String},
              leadSqrCreateId:{type:String},
              createNotificationStatus:{type:Boolean,default:0},
              responseNotificationStatus:{type:Boolean,default:0},
              leadSquareResObj:{type:Object},
}, { timestamps: {} });

var model = mongoose.model('allQuery', allQuerySchema);

module.exports = model