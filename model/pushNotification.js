var mongoose = require('mongoose');
//Define a schema
var Schema = mongoose.Schema;
var pushNnotificationModelSchema = new Schema({
    mobileNo:Number,
    deviceId:String,
    // tittle:String,
    // description:String,
    type:String,
    typeNotificationID:String,
    status: {type: Boolean, default: 0},
}, { timestamps: {} });
var model=mongoose.model('pushNotification',pushNnotificationModelSchema);


module.exports=model