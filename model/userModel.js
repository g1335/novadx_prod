var mongoose = require('mongoose');
//Define a schema
var Schema = mongoose.Schema;
var userModelSchema = new Schema({
    deviceId:String,
    firstName: String,
    dateofBirth: String,
    gender: String,
    mobileNo: {
        type: Number,
        unique: true, // This field must be unique
        required: true, // This field is required
      },
    email: String,
    otp: Number,
    password: String,
    image: String,
    role: Number,
},{timestamps:{}});
var model=mongoose.model('user',userModelSchema);


module.exports=model