var mongoose = require('mongoose');
//Define a schema
var Schema = mongoose.Schema;
var notificationModelSchema = new Schema({
    mobileNo:Number,
    deviceId:String,
    appName:String,
    model:String,
    appId:String,
    registrationToken: String
}, { timestamps: {} });
var model=mongoose.model('notification',notificationModelSchema);


module.exports=model