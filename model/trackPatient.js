var mongoose = require('mongoose');
//Define a schema
var Schema = mongoose.Schema;
var patientModelSchema = new Schema({
        mobileNo: {type: Number, default: 0},
        age: {type: Number, default: 0},
        gender:{type:String,default:0},
       }, { timestamps: {} });
var model = mongoose.model('trackPatient', patientModelSchema);

module.exports = model