var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var queryModelSchema = new Schema({
    mobileNo:Number, 
    queryType:String,
    queryContent:String
         
}, { timestamps: {} });

var model = mongoose.model('query', queryModelSchema);

module.exports = model