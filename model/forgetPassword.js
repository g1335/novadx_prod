var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var loginModelSchema = new Schema({
          mobileNo:Number, 
          newPassword:String,
          confirmPassword:String
}, { timestamps: {} });

var model = mongoose.model('forgetPassword', loginModelSchema);

module.exports = model