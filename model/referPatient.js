var mongoose = require('mongoose');
//Define a schema
var Schema = mongoose.Schema;
var patientModelSchema = new Schema({
    docNumber: Number,
    name: String,
    age: Number,
    gender: String,
    mobileNo: Number,
    email: String,
    city: String,
    preferreddoctorremarks:String
},{timestamps:{}});

var model=mongoose.model('referPatient',patientModelSchema);

module.exports=model