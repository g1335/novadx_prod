var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var loginModelSchema = new Schema({
          mobileNo:Number, 
          password:String,
          isDeleted:Number// 0 false 1 true
}, { timestamps: {} });

var model = mongoose.model('user', loginModelSchema);

module.exports = model