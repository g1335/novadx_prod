var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var novaNewsModelSchema = new Schema({
    title:{type:String},
    description:{type:String},
    publishedDate:{type:Date},
    url:{type:String},
    image:{type:String},
}, { timestamps: {} });

var model = mongoose.model('novaNews', novaNewsModelSchema);

module.exports = model