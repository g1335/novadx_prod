const winston = require('winston');
var moment = require('moment');
var now = moment()
var currentDate = now.format('YYYYMMDD');

const logger = winston.createLogger({
  //level: 'error',
  format: winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSSZZ' }),
    winston.format.json()
  ),
  //defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.File({ filename: 'Logs/' + currentDate + '/error.log', level: 'error' })
  ],
});

module.exports = logger;