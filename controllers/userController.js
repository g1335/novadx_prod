const userService = require('../services/userService')
const util = require('../util/token')
const fs = require('fs');

exports.otpTrigger = (req, res) => {
    try {
      req.checkBody('mobileNo', 'Mobile Number is required').not().isEmpty();
      var errors = req.validationErrors();
      var response = {};
  
      if (errors) {
        response.success = false;
        response.error = errors;
        return res.status(422).send(response);
      } else {
        userService.otpTrigger(req, (err, result) => {
          if (err) {
            return res.status(500).send({
              message: err
            });
          } else {
            response.success = true;
            response.result = "OTP sent Successfully";
            response.success = result;
            return res.status(200).send(response);
          }
        });
      }
    } catch (error) {
      console.error(error);
      return res.status(500).send({
        message: error.message
      });
    }
  };
  
exports.getNovaNewsNotification = (req, res) =>{
    console.log('hello world');
}
exports.login = (req, res) => {
    try {
        req.checkBody('deviceId', 'Device Id is required').not().isEmpty();
        req.checkBody('mobileNo', 'Mobile Number is required').not().isEmpty();
        // req.checkBody('password', 'Password is required').not().isEmpty();
        var errors = req.validationErrors();
        var response = {};
        if (errors) {
            response.success = false;
            response.error = errors;
            // console.log(error);
            return res.status(422).send(response);
        }
        else {
            // var obj = {
            //     deviceId: req.body.deviceId

            // }
            userService.login(req, (err, result) => {
                if (err) {
                    return res.status(500).send({
                        message: err
                    });
                }
                else {
                    response.success = true;
                    response.result = "Login Successfully";
                    // response.success = result;
                    const payload = {
                        mobileNo: req.body.mobileNo
                    }
                    const obj = util.GenerateTokenAuth(payload);
                    response.success = true;
                    response.token = obj;
                    response.mobileNo = result.mobileNo;
                    return res.status(200).send(response);
                }
            })

        }
    } catch (error) {
        res.send(error)
    }
}

exports.registration = (req, res) => {
    try {
        req.checkBody("firstName", "FirstName is required").not().isEmpty();
        // req.checkBody("dateofBirth", "DateofBirth is required").not().isEmpty();
        req.checkBody("gender", "Gender is required").not().isEmpty();
        req.checkBody("mobileNo", "MobileNumber is required").not().isEmpty();
        req.checkBody("email", "Email is required").not().isEmpty();
        req.checkBody("password", "New Password is required").not().isEmpty();
        req.checkBody("reenterPassword", "this field is required").not().isEmpty();

        var responseResult = {}
        var errors = req.validationErrors()
        if (errors) {
            responseResult.status = false;
            responseResult.error = errors
            return res.send(responseResult)
        } else {
            var response = {}
            userService.registration(req, (err, result) => {
                if (err) {
                    console.log(err);
                    response.status = false;
                    response.error = err
                    return res.send(response)
                }
                else {
                    response.success = true;
                    response.result = "Registration successful";
                    response.status = true;
                    response.userId = result.createdUserId;
                    return res.status(200).send(response)
                }
            })
        }
    } catch (error) {
        res.send(error)
    }
}


exports.forgetPassword = (req, res) => {
    try {
        req.checkBody("mobileNo", "MobileNumber is required").not().isEmpty();
        req.checkBody("newPassword", "New Password is required").not().isEmpty();
        req.checkBody("confirmPassword", "Confirm Password is required").not().isEmpty();

        var responseResult = {}
        var errors = req.validationErrors()
        if (errors) {
            responseResult.status = false;
            responseResult.error = errors
            return res.send(responseResult)
        } else {
            var response = {}
            userService.forgetPassword(req, (err, result) => {
                if (err) {

                    response.status = false;
                    response.error = err
                    return res.send(response)
                }
                else {
                    response.status = true;
                    response.result = "Password Reset Successful";
                    response.userId = result.createdUserId;
                    return res.status(200).send(response)
                }
            })
        }
    } catch (error) {
        res.send(error)
    }
}

exports.myProfile = (req, res) => {
    try {
        req.checkBody("mobileNo", "MobileNumber is required").not().isEmpty();

        var responseResult = {}
        var errors = req.validationErrors()
        if (errors) {
            responseResult.status = false;
            responseResult.error = errors
            return res.send(responseResult)
        } else {
            var response = {}
            userService.myProfile(req, (err, result) => {
                if (err) {

                    response.status = false;
                    response.error = err
                    return res.send(response)
                }
                else {
                    response.status = true;
                    response.result = result;
                    return res.status(200).send(response)
                }
            })
        }
    } catch (error) {
        res.send(error)
    }
}
exports.updateUser = (req, res) => {
    try {
        req.checkBody("firstName");
        req.checkBody("dateofBirth");
        req.checkBody("gender");
        req.checkBody("email");
        req.checkBody("mobileNo");

        var responseResult = {}
        var errors = req.validationErrors()
        if (errors) {
            responseResult.status = false;
            responseResult.error = errors
            return res.send(responseResult)
        } else {
            var response = {}
            userService.updateUser(req, (err, result) => {
                if (err) {

                    response.status = false;
                    response.error = err
                    return res.send(response)
                }
                else {
                    response.status = true;
                    response.result = result;
                    return res.status(200).send(response)
                }
            })
        }
    } catch (error) {
        res.send(error)
    }
}

exports.deleteUser = (req, res) => {
    try {
        req.checkBody("mobileNo", "MobileNumber is required").not().isEmpty();
        var responseResult = {}
        var errors = req.validationErrors()
        if (errors) {
            responseResult.status = false;
            responseResult.error = errors
            return res.send(responseResult)
        } else {
            var response = {}
            userService.deleteUser(req, (err, result) => {
                if (err) {

                    response.status = false;
                    response.error = err
                    return res.send(response)
                }
                else {
                    response.status = true;
                    response.result = result;
                    return res.status(200).send(response)
                }
            })
        }

    } catch (error) {
        res.send(error)
    }
}

exports.createQuery = async (req, res) => {

    try {

        req.checkBody('docNumber', 'docNumber Id is required').not().isEmpty();
        req.checkBody('docName', 'docName Number is required').not().isEmpty();
        req.checkBody('queryType', 'queryType is required').not().isEmpty();
        req.checkBody('queryContent', 'queryContent is required').not().isEmpty();


        var responseResult = {}
        var errors = req.validationErrors()
        if (errors) {
            responseResult.status = false;
            responseResult.error = errors
            return res.send(responseResult)
        } else {
            var response = {}
            userService.createQuery(req, (err, result) => {
                if (err) {
                    response.status = false;
                    response.error = err
                    return res.send(response)
                } else {
                    response.status = true;
                    response.data = result;
                    return res.status(200).send(response)
                }
            })
        }

    } catch (error) {
        res.send(error)
        // console.log(error)
    }
}
// exports.uploadProfilePicToAzure = (req, res) => {
//     req.checkBody("mobileNo", "MobileNumber is required").not().isEmpty();
//     req.checkBody("file", "Image is required").isEmpty();
//     try {
//         var responseResult = {}
//         var errors = req.validationErrors()
//         if (errors) {
//             responseResult.status = false;
//             responseResult.error = errors
//             return res.status(422).send(responseResult)
//         } else {
//             var response = {}
//             userService.uploadProfilePicToAzure(req, (err, result) => {
//                 if (err) {
//                     response.status = false;
//                     response.error = err
//                     return res.status(500).send(response)
//                 } else {
//                     response.status = true;
//                     response.message = 'Profile pic uploaded successfuly!'
//                     response.imageUrl = result;
//                     return res.status(200).send(response)
//                 }
//             })
//         }

//     } catch (error) {
//         res.send(error)
//         console.log(error)
//     }
// }

exports.getProfilePic = (req, res) => {

    req.checkBody("mobileNo", "MobileNumber is required").not().isEmpty();
    try {
        var responseResult = {}
        var errors = req.validationErrors()
        if (errors) {
            responseResult.status = false;
            responseResult.error = errors
            return res.status(422).send(responseResult)
        } else {
            var response = {}
            userService.getProfilePic(req, (err, result) => {
                if (err) {
                    response.status = false;
                    response.error = err
                    return res.status(500).send(response)
                } else {
                    response.status = true;
                    response.result = result;
                    response.success = 'Image uploaded successfully.';
                    return res.status(200).send(response)
                }
            })
        }

    } catch (error) {
        res.send(error)
        console.log(error)
    }
}



exports.notification = (req, res) => {
    try {
        req.checkBody("registrationToken", "registrationToken is required").not().isEmpty();
        var responseResult = {}
        var errors = req.validationErrors()
        if (errors) {
            responseResult.status = false;
            responseResult.error = errors
            console.log(errors);
            return res.status(422).send(responseResult)
        } else {
            var response = {}
            userService.notification(req, (err, result) => {
                if (err) {
                    console.log("Hi...err", err);
                    return res.status(500).send(err)
                }
                else {
                    console.log("Hi...res", result);
                    return res.status(200).send(result)
                }
            })
        }
    } catch (error) {
        res.send(error)
    }
}

exports.pushNotification = (req, res) => {
    try {
        req.checkBody("title", "Title is required").not().isEmpty();
        req.checkBody("body", "Body is required").not().isEmpty();
        // req.checkBody("mobileNumber", "Mobile Number is required").not().isEmpty();
        var responseResult = {}
        var errors = req.validationErrors()
        if (errors) {
            responseResult.status = false;
            responseResult.error = errors
            console.log(errors);
            return res.status(422).send(responseResult)
        } else {
            var response = {}
            userService.pushNotification(req, (err, result) => {
                if (err) {
                    console.log(err);
                    response.status = false;
                    response.error = err
                    return res.status(500).send(response)
                }
                else {
                    response.status = true;
                    response.result = "Notification sent successfully!";
                    return res.status(200).send(response)
                }
            })
        }
    } catch (error) {
        res.send(error)
    }
}

exports.getNotification = (req, res) => {
    try {
        req.checkBody("deviceId", "Title is required").not().isEmpty();
        req.checkBody("type", "Body is required").not().isEmpty();
        req.checkBody("mobileNo", "Body is required").not().isEmpty();
        // req.checkBody("mobileNumber", "Mobile Number is required").not().isEmpty();
        var responseResult = {}
        var errors = req.validationErrors()
        if (errors) {
            responseResult.status = false;
            responseResult.error = errors
            console.log(errors);
            return res.status(422).send(responseResult)
        } else {
            var response = {}
            userService.getNotification(req, (err, result) => {
                if (err) {
                    console.log(err);
                    response.status = false;
                    response.error = err
                    return res.status(500).send(response)
                }
                else {
                    response.status = true;
                    response.result = "Notification sent successfully!";
                    return res.status(200).send(response)
                }
            })
        }
    } catch (error) {
        res.send(error)
    }
}

exports.loginByDeviceId = (req, res) => {
    try {
        req.checkBody('deviceId', 'Device Id is required').not().isEmpty();

        var errors = req.validationErrors();
        var response = {};
        if (errors) {
            response.success = false;
            response.error = errors;
            return res.status(422).send(response);
        }
        else {

            userService.loginByDeviceId(req, (err, result) => {
                if (err) {
                    return res.status(500).send({
                        message: err
                    });
                }
                else {
                    response.success = true;
                    response.result = "Login Successfully";
                    response.successData = result;
                    const payload = {
                        mobileNo: req.body.mobileNo
                    }
                    const obj = util.GenerateTokenAuth(payload);
                    // response.success = true;
                    response.token = obj;
                    response.mobileNo = result.mobileNo;
                    return res.status(200).send(response);
                }
            })

        }
    } catch (error) {
        res.send(error)
    }
}