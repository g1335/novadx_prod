const doctorService=require('../services/doctorService');
const util = require('../util/token')

exports.getDoctorDetailsBymobileNo = (req, res) => {
    req.checkBody("mobileNo", "mobileNo is required").not().isEmpty();

    var doctorObj = [
        {firstName:"maina"}
    ]
return res.status(200).send(doctorObj)

    
    try {
        var responseResult = {}
        var errors = req.validationErrors()
        if (errors) {
            responseResult.status = false;
            responseResult.error = errors
            return res.status(422).send(responseResult)
        } else {
            var response = {}
            doctorService.getDoctorDetailsBymobileNo(req, (err, result) => {
                if (err) {
                    response.status = false;
                    response.error = err
                    return res.status(500).send(response)
                } else {
                    response.success = true;
                    response.status = true;
                    response.data = result;
                    return res.status(200).send(response)
                }
            })
        }

    } catch (error) {
        res.send(error)
        console.log(error)
    }
}