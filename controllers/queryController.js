
const allQuery=require('../services/allQueryService')
const mysql = require('mysql');

// get getQueryResponse from leadsquare API
exports.getQueryResponse = async (req, res) => {
  try{
      req.checkBody('id','Id field is required').not().isEmpty();
      req.checkBody('response', 'response field is required').not().isEmpty();
      console.log(req);
      var responseResult = {}
      var errors = req.validationErrors()
      if (errors) {
          responseResult.status = false;
          responseResult.error = errors
          return res.send(responseResult)
      } else {
          var response = {}
          allQuery.getQueryResponse(req, (err, result) => {
              if (err) {
                  response.status = false;
                  response.error = err
                  return res.send(response)
              } else {
                  response.status = true;
                  response.data = result;
                  return res.status(200).send(response)
              }
          })
      }
  } catch (error) {
      res.send(error)
      // console.log(error)
  }
}

exports.allQuery = (req, res) => {
  try {
    req.checkBody("mobileNo", "MobileNumber is required").not().isEmpty();
    var errors = req.validationErrors();
    var response = {};
    if (errors) {
      response.success = false;
      response.error = errors;
      return res.send(response);
    }else{
      var response = {}
      allQuery.allQuery(req, (err, result) => {
        if (err) {
          response.status = false;
          response.error = err
          return res.send(response)
        }
        else {
          response.status = true;
          response.result = result;
          console.log(result);
          return res.status(200).send(response)
        }
      })
    }
  } catch (error) {
    res.send(error)
  }
}