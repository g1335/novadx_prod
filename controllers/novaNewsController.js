const novaNewsService=require('../services/novaNewsService')
const util = require('../util/token')

exports.getAllNews = (req, res) => {
    try {
        var response = {};
       
        novaNewsService.getAllNews(req, (err, result) => {
                if (err) {
                                
                    response.status = false;
                    response.error = err
                    return res.send(response)
                }
                else {
                 
                    response.status = true;
                    response.result = result;
                    return res.status(200).send(response)
                }
            })
        
    } catch (error) {
        res.send(error)
    }
  }