const trackPatientService = require('../services/trackPatientService')
// const allQuery=require('../services/allQueryService')
const referPatientService = require('../services/referPatientService')
const referPatient = require('../model/referPatient');
// const mySql = require('../config/MySQL');
const mysql = require('mysql');

exports.referPatient = (req, res) => {
  try {
    req.checkBody("name", "Name is required").not().isEmpty();
    req.checkBody("age", "Age is required").not().isEmpty();
    req.checkBody("gender", "Gender is required").not().isEmpty();
    req.checkBody("mobileNo", "MobileNumber is required").not().isEmpty();
    req.checkBody("email", "Email is required").not().isEmpty();
    req.checkBody("city", "City").not().isEmpty();
    req.checkBody("preferreddoctorremarks", "preferreddoctorremarks").not().isEmpty();

    var responseResult = {}
    var errors = req.validationErrors()
    if (errors) {
      responseResult.status = false;
      responseResult.error = errors
      return res.send(responseResult)
    } else {
      var response = {}
      referPatientService.referPatient(req, (err, result) => {
        if (err) {
          response.status = false;
          response.error = err
          return res.send(response)
        }
        else {
          response.status = true;
          response.resultMsg = "Referred Patient Sucessfully";
          response.result = result;
          return res.status(200).send(response)
        }
      })
    }
  } catch (error) {
    res.send(error)
  }
}

var statuses = {
  "0": "Active",
  "4": "Released",
  "5": "Cancelled",
  "-1": "Scheduled",
  "-2": "Requested",
  "2": "Marked For Discharge",
  "1": "Patient Arrived",
  "3": "Discharged",
  "6": "Closed",
};

exports.trackPatient = (req, res) => {
  var mbl = req.body.mobileNo;
  try {
    req.checkBody("mobileNo", "MobileNumber is required").not().isEmpty();
    var errors = req.validationErrors();
    var response = {};
    if (errors) {
      response.success = false;
      response.error = errors;
      return res.send(response);
    }

    const query = `SELECT pd.rp_pat_rid, pd.rp_pat_full_name, pd.rp_pat_phone, pd.rp_pat_age, pd.rp_pat_gender_name, vd.rv_visit_referral_phone_no, vd.rv_visit_create_datetime, vd.rv_visit_status 
          FROM rpt_visit_details AS vd 
          LEFT JOIN medics_report_db.rpt_patient_details AS pd 
          ON vd.rv_visit_patient_rid = pd.rp_pat_rid 
          WHERE vd.rv_visit_referral_phone_no = '${mbl}' AND vd.rv_visit_status >=0 AND vd.rv_visit_status <>5 GROUP BY pd.rp_pat_phone`;
    const connection = mysql.createConnection({
      host: '14.143.132.49',
      user: 'novadx',
      password: 'NovaDx@3674',
      database: 'medics_report_db'
    });
    var response = {};
    connection.connect((err, req) => {
      if (err) {
        console.log('Eror connecting');
      } else {
        connection.query(query, function (err, result, fields) {
          if (err) throw err;
          response.statuses = statuses;
          response.biResult = result;
        });
      }

    });
    referPatient.find({ docNumber: mbl })
      .then(count => {
        if (count.length > 0) {
          response.mongoResult = count;
          return res.status(200).send(response)
        } else {
          return res.status(200).send(response)
        }
      });
  } catch (error) {
    res.send(error)
  }
}



exports.trackPatientDetails = (req, res) => {
  var patientID = req.body.patientID;
  try {
    req.checkBody("patientID", "patientID is required").not().isEmpty();
    var errors = req.validationErrors();
    var response = {};
    if (errors) {
      response.success = false;
      response.error = errors;
      return res.send(response);
    }

    // const query = `SELECT pd.*,dd.rv_visit_consulting_doctor_name FROM rpt_patient_details AS pd LEFT JOIN rpt_visit_details AS dd on pd.rp_pat_rid=dd.rv_visit_patient_rid WHERE pd.rp_pat_rid = '${patientID}' GROUP BY pd.rp_pat_rid`;
    const query = `SELECT
                      clin.tc_rid,
                      clin.tc_clinical_pregnancy_recorded_date,
                      clin.tc_clinical_pregnancy_beta_hcgvalue,
                      clin.tc_biochemical_pregnancy_recorded_date,
                      clin.tc_biochemical_pregnancy_beta_hcgvalue,
                      clin.tc_no_pregnancy_recorded_date,
                      clin.tc_no_pregnancy_beta_hcgvalue,
                      tc.Doctor,
                      tc.Treatment_Cycle_Id,
                      tc.Treatment_No,
                      tc.Treatment_Type,
                      tc.Patient_MRN,
                      tc.Cycle_Start_Date,
                      tc.OPU_Done_On,
                      tc.IUI_Done_On,
                      tc.Pregnancy_Outcome,
                      tc.No_of_initial_sacs,
                      tc.Final_Outcome,
                      tc.tc_modified_date_time,
                      pd.*
                  FROM
                      rpt_patient_details AS pd
                  LEFT JOIN
                      medics_report_db.rpt_treatment_details AS tc ON pd.rp_pat_mrn = tc.Patient_MRN
                  LEFT JOIN
                      medics_report_db.clin_art_treatment_cycle AS clin ON tc.Treatment_Cycle_Id = clin.tc_rid
                  WHERE
                  pd.rp_pat_rid='${patientID}'`;
    const connection = mysql.createConnection({
      host: '14.143.132.49',
      user: 'novadx',
      password: 'NovaDx@3674',
      database: 'medics_report_db'
    });

    connection.connect((err, req) => {
      if (err) {
        console.log('Eror connecting');
      } else {
        connection.query(query, function (err, result, fields) {
          if (err) throw err;
          response.statuses = statuses;
          response.result = result;
          return res.status(200).send(response)
        });
      }

    });

  } catch (error) {
    res.send(error)
  }
}


